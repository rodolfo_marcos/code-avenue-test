var geolocationApp = angular.module('geolocation', []);

geolocationApp.controller('geolocationCtrl', function ($scope, $http) {
  
	// Inicializa Leaflet
	var map = new L.Map('map', {
		center: new L.LatLng(51.505, -0.09),
		zoom: 8,
		layers: new L.TileLayer('https://a.tiles.mapbox.com/v3/mapbox.world-bright/{z}/{x}/{y}.png')
	});

	$scope.local = "Obtendo localização. Libere o rasteramento de seu novegador.";
	// Obtém localização
	navigator.geolocation.getCurrentPosition(showPosition);

	function showPosition(position){
    	var ponto = new L.LatLng(position.coords.latitude , position.coords.longitude);
		map.setView(ponto);
	}

	$scope.teste = "Olá isto é um teste";

	$scope.submit = function(){
		var url = "http://ip-api.com/json/" + $scope.dominio;
		$http({
			method: 'GET',
			url: url
		}).then(function successCallback(response) {
			var dados = response.data;
			var ponto = new L.LatLng(dados.lat, dados.lon);
			map.setView(ponto);
			var marker = new L.Marker(ponto);
			marker.bindPopup('Aqui está ' + dados.as);
			map.addLayer(marker);
			// this callback will be called asynchronously
			// when the response is available
			}, function errorCallback(response) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			});
	}
});