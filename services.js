// Serviço que obtém localização
geolocationApp.factory("apiHost",function($http){
	return{
		obtemLocalizacao: function(dominio){
			var url = "http://ip-api.com/json/" + dominio;
			return $http({
				method: 'GET',
				url: url
			});
		}
	}
});